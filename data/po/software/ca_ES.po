# 
# Translators:
# Robert Antoni Buj Gelonch <rbuj@fedoraproject.org>, 2016
msgid ""
msgstr ""
"Project-Id-Version: Ubuntu MATE Welcome\n"
"POT-Creation-Date: 2016-02-27 12:48+0100\n"
"PO-Revision-Date: 2016-10-24 07:58+0000\n"
"Last-Translator: Robert Antoni Buj Gelonch <rbuj@fedoraproject.org>\n"
"Language-Team: Catalan (Spain) (http://www.transifex.com/ubuntu-mate/ubuntu-mate-welcome/language/ca_ES/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: ca_ES\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"Report-Msgid-Bugs-To : you@example.com\n"

#: software.html:18
msgid "Software"
msgstr "Programari"

#: software.html:18
msgid "Boutique"
msgstr "Botiga"

#: software.html21, 21
msgid "Bulk Queue"
msgstr "Cua"

#: software.html:22
msgid "Bulk Queue Disabled"
msgstr "Cua inhabilitada"

#: software.html23, 23
msgid "Search"
msgstr "Cerca"

#: software.html24, 24, 344
msgid "Boutique News"
msgstr "Notícies de la botiga"

#: software.html25, 25, 375
msgid "Preferences"
msgstr "Preferències"

#: software.html33, 33, 139
msgid "Accessories"
msgstr "Accessoris"

#: software.html34, 34, 152
msgid "Education"
msgstr "Educació"

#: software.html35, 35, 165
msgid "Games"
msgstr "Jocs"

#: software.html36, 36, 178
msgid "Graphics"
msgstr "Gràfics"

#: software.html37, 37, 191
msgid "Internet"
msgstr "Internet"

#: software.html38, 38, 204
msgid "Office"
msgstr "Ofimàtica"

#: software.html39, 39, 217
msgid "Programming"
msgstr "Programació"

#: software.html40, 40, 230
msgid "Sound & Video"
msgstr "So i vídeo"

#: software.html41, 41, 243
msgid "System Tools"
msgstr "Eines del sistema"

#: software.html42, 42, 256
msgid "Universal Access"
msgstr "Accés universal"

#: software.html43, 43, 266
msgid "Servers"
msgstr "Servidors"

#: software.html44, 44
msgid "More Software"
msgstr "Més programari"

#: software.html45, 45
msgid "Fixes"
msgstr "Correccions"

#: software.html:54
msgid "Software Boutique"
msgstr "Botiga de programari"

#: software.html:55
msgid ""
"There is an abundance of software available for Ubuntu MATE and some people "
"find that choice overwhelming. The Boutique is a carefully curated selection"
" of the best-in-class applications chosen because they integrate well, "
"complement Ubuntu MATE and enable you to self style your computing "
"experience."
msgstr "Hi ha una gran quantitat de programari disponible per a Ubuntu MATE i algunes persones troben aclaparadora l'elecció. La botiga és una selecció acurada de les millors aplicacions en la seva classe triades perquè s'integren bé, complementen Ubuntu MATE i us permeten un estil propi per a la vostra experiència informàtica."

#: software.html:62
msgid "If you can't find what you're looking for, install one of the"
msgstr "Si no podeu trobar allò que esteu buscant, instal·leu un dels"

#: software.html63, 361
msgid "Get More Apps"
msgstr "Obté més aplicacions"

#: software.html:64
msgid "software centers"
msgstr "centres de programari"

#: software.html:65
msgid "to explore the complete Ubuntu software catalog."
msgstr "per explorar el catàleg complet de programari d'Ubuntu."

#: software.html:69
msgid "More Apps Await."
msgstr "Més aplicacions esperen."

#: software.html:70
msgid ""
"Connect your computer to the Internet to explore a wide selection of "
"applications for Ubuntu MATE."
msgstr "Connecteu el vostre ordinador a Internet per explorar una àmplia selecció d'aplicacions per a Ubuntu MATE."

#: software.html:72
msgid ""
"Once online, you'll be able to download and install tried & tested software "
"right here. Our picks ensure that the featured software integrates well on "
"the Ubuntu MATE desktop."
msgstr "Un cop estigueu en línia, aquí podreu baixar i instal·lar el programari provat i revisat. Les nostres seleccions asseguren que el programari destacat s'integra bé a l'escriptori Ubuntu MATE."

#: software.html:78
msgid ""
"Sorry, Welcome could not establish a connection to the Ubuntu repository "
"server. Please check your connection and try again."
msgstr "Ho sentim, la benvinguda no ha pogut establir una connexió amb el servidor de dipòsits d'Ubuntu. Si us plau, comproveu la vostra connexió i torneu-ho a intentar."

#: software.html:83
msgid "Retry Connection"
msgstr "Torna a intentar la connexió"

#: software.html:86
msgid "Connection Troubleshooting"
msgstr "Solucionador de problemes de la connexió"

#: software.html:87
msgid "Connection Help"
msgstr "Ajuda de la connexió"

#: software.html:93
msgid "Get Connected."
msgstr "Establiment de la connexió."

#: software.html:94
msgid ""
"Here's a few things you can check, depending on the type of connection you "
"have:"
msgstr "Aquí hi ha algunes coses que podeu comprovar, en funció del tipus de connexió que tingueu:"

#: software.html:95
msgid "Wired Connection"
msgstr "Connexió amb fil"

#: software.html:97
msgid "Is the cable securely plugged in?"
msgstr "Està el cable fermament endollat?"

#: software.html:98
msgid ""
"Is the router online and can other devices access the network? Try "
"restarting the router."
msgstr "Està l'encaminador en línia i poden els altres dispositius accedir a la xarxa? Proveu en reiniciar l'encaminador."

#: software.html:101
msgid "Wireless Connection"
msgstr "Connexió sense fil"

#: software.html:102
msgid "Have you entered the correct wireless password from the"
msgstr "Heu introduït la contrasenya correcta sense fil des de"

#: software.html:102
msgid "network indicator"
msgstr "indicador de xarxa"

#: software.html:102
msgid "in the upper-right?"
msgstr "a la part superior dreta?"

#: software.html:104
msgid "When disconnected, the applet looks like this:"
msgstr "Quan us desconnecteu, la miniaplicació es veu així:"

#: software.html:106
msgid ""
"Is there a physical switch that may have accidentally switched off wireless "
"functionality?"
msgstr "Hi ha algun interruptor físic que hagi pogut apagar accidentalment la funcionalitat sense fils?"

#: software.html:108
msgid "Is there a soft (keyboard) switch that toggles it off?"
msgstr "Hi ha algun interruptor de programari (teclat) que ho apaga?"

#: software.html:110
msgid "You may need to hold the FN key while pressing the function key."
msgstr "És possible que necessiteu mantenir premuda la tecla FN mentre premeu la tecla de la funció."

#: software.html:111
msgid "Here's an example:"
msgstr "Aquí hi ha un exemple:"

#: software.html:115
msgid "Not working at all or experiencing sluggish connections?"
msgstr "No funciona del tot o experimenteu connexions lentes?"

#: software.html:116
msgid ""
"Sorry to hear that. You will need a temporary wired connection to install "
"working drivers."
msgstr "Em sap greu escoltar això. Necessitareu una connexió amb fil temporal per instal·lar els controladors que funcionin."

#: software.html:118
msgid "The"
msgstr "La pestanya de"

#: software.html:118
msgid "Additional Drivers"
msgstr "controladors addicionals"

#: software.html:118
msgid "tab may have them available for your system."
msgstr "pot tenir-los disponibles per al vostre sistema."

#: software.html:119
msgid ""
"Otherwise, you will need to manually install third party drivers for your "
"hardware, or a download specific package containing the firmware. See"
msgstr "Altrament, haureu d'instal·lar manualment els controladors de tercers per al vostre maquinari, o un paquet específic baixat que contingui el microprogramari. Vegeu els"

#: software.html:120
msgid "Drivers"
msgstr "controladors"

#: software.html:120
msgid "in the"
msgstr "a la secció"

#: software.html:120
msgid "Getting Started"
msgstr "com començar"

#: software.html:121
msgid "section for more details."
msgstr "per a més detalls."

#: software.html:122
msgid "Feel free to"
msgstr "Sentiu-vos lliures per"

#: software.html:122
msgid "ask the community"
msgstr "preguntar a la comunitat"

#: software.html:122
msgid "if you need assistance."
msgstr "si necessiteu ajuda."

#: software.html:126
msgid "Our Picks"
msgstr "Les nostres seleccions"

#: software.html:127
msgid "OK"
msgstr "D'acord"

#: software.html141, 154, 167, 180, 193, 206, 219, 232, 245
msgid "No Filter"
msgstr "Cap filtre"

#: software.html:143
msgid "Handy utilities for your computing needs."
msgstr "Les utilitats pràctiques per a les vostres necessitats informàtiques."

#: software.html:156
msgid "For study and children."
msgstr "Per als estudis i els nens."

#: software.html:169
msgid "A selection of 2D and 3D games for your enjoyment."
msgstr "Una selecció de jocs en 2D i en 3D per al vostre goig."

#: software.html:182
msgid "For producing and editing works of art."
msgstr "Per a la producció i edició d'obres artístiques."

#: software.html:195
msgid "For staying connected and enjoying the features of your own cloud."
msgstr "Per romandre connectat i gaudir de les característiques del vostre propi núvol."

#: software.html:208
msgid "For more then just documents and spreadsheets."
msgstr "Per a més que simples documents i fulls de càlcul."

#: software.html:221
msgid "For the developers and system administrators out there."
msgstr "Per als desenvolupadors i administradors de sistemes."

#: software.html:234
msgid "Multimedia software for listening and production."
msgstr "Programari multimèdia per a l'escolta i la producció."

#: software.html:247
msgid "Software that makes the most out of your system resources."
msgstr "El programari que crea la majoria dels recursos del vostre sistema."

#: software.html:257
msgid "Software that makes your computer more accessible."
msgstr "El programari que fa que el vostre ordinador sigui més accessible."

#: software.html:267
msgid "One-click installations for serving the network."
msgstr "Instal·lacions d'un sol clic per servir a la xarxa."

#: software.html:276
msgid "Discover More Software"
msgstr "Descobriu més programari"

#: software.html:277
msgid ""
"Graphical interfaces to browse a wide selection of software available for "
"your operating system."
msgstr "Les interfícies gràfiques per a navegar per una àmplia selecció de programari disponible per al vostre sistema operatiu."

#: software.html:285
msgid "Miscellaneous Fixes"
msgstr "Miscel·lània de les correccions"

#: software.html:286
msgid ""
"This section contains operations that can fix common problems should you "
"encounter an error while upgrading or installing new software."
msgstr "Aquesta secció conté les operacions que poden corregir els problemes comuns si us trobeu amb un error mentre actualitzeu o instal·leu programari nou."

#: software.html:293
msgid "Outdated Package Lists?"
msgstr "Llistes obsoletes de paquets?"

#: software.html:294
msgid "Your repository lists may be out of date, which can cause"
msgstr "Les vostres llistes de dipòsits poden estar obsoletes, el que pot causar els errors"

#: software.html:294
msgid "Not Found"
msgstr "No trobat"

#: software.html:294
msgid "errors and"
msgstr "i"

#: software.html:294
msgid "outdated version"
msgstr "versió obsoleta"

#: software.html:295
msgid ""
"information when trying to install new or newer versions of software. This "
"is particularly the case when Ubuntu MATE connects online for the first time"
" after installation."
msgstr "quan s'intenten instal·lar versions noves de programari o programari nou. Aquest és el cas concret quan Ubuntu MATE es connecta en línia per primera vegada després de la instal·lació."

#: software.html:298
msgid "Update the repository lists:"
msgstr "Actualitza els llistats dels dipòsits:"

#: software.html:302
msgid "Update Sources List"
msgstr "Actualitza la llista dels orígens"

#: software.html:303
msgid "Upgrade Installed Packages"
msgstr "Actualitza els paquets instal·lats"

#: software.html:307
msgid "Broken Packages?"
msgstr "Paquets trencats?"

#: software.html:308
msgid "When a previous installation or removal was interrupted"
msgstr "Quan s'ha interromput una instal·lació o eliminació prèvia"

#: software.html:309
msgid "(for instance, due to power failure or loss of Internet connection)"
msgstr "(per exemple, a causa d'un tall del corrent o d'una pèrdua de la connexió a Internet)"

#: software.html:310
msgid ""
"further software cannot be added or removed without properly re-configuring "
"these broken packages. If necessary, you may need to also resolve "
"dependencies and install any missing packages in order for the software to "
"run properly."
msgstr "no es pot afegir o suprimir més programari sense reconfigurar adequadament aquests paquets trencats. Si és necessari, pot ser que hàgiu de resoldre també les dependències i instal·lar els paquets que falten perquè el programari s'executi correctament."

#: software.html:316
msgid "Configure packages that were unpacked but not configured:"
msgstr "Configura paquets que s'han desempaquetat però no s'han configurat:"

#: software.html:320
msgid "Download and install broken dependencies:"
msgstr "Baixa i instal·la les dependències trencades:"

#: software.html:324
msgid "Configure Interrupted Packages"
msgstr "Configura els paquets interromputs"

#: software.html:325
msgid "Resolve Broken Packages"
msgstr "Resol els paquets trencats"

#: software.html:329
msgid ""
"In addition, listed below are terminal equivalent commands that otherwise "
"appear in the"
msgstr "A més, s'enumeren a continuació les ordres equivalents del terminal que altrament apareixen en l'"

#: software.html:330
msgid "Software Updater"
msgstr "actualitzador de programari"

#: software.html:332
msgid "Upgrade all packages that have a new version available:"
msgstr "Actualitza tots els paquets que tenen disponible una versió nova:"

#: software.html:336
msgid "Upgrade to a new release of Ubuntu MATE:"
msgstr "Actualitza a una nova versió d'Ubuntu MATE:"

#: software.html:345
msgid ""
"Keeping you informed of the changes made to Ubuntu MATE's Software Boutique."
msgstr "Us mantenim informat dels canvis realitzats en la botiga de programari d'Ubuntu MATE."

#: software.html:350
msgid "Search for Applications"
msgstr "Cerca aplicacions"

#: software.html:351
msgid "Enter a keyword to search the Software Boutique."
msgstr "Introduïu una paraula clau per cercar a la botiga de programari."

#: software.html:353
msgid "Name of Application"
msgstr "Nom de l'aplicació"

#: software.html:358
msgid "No software matched your key words."
msgstr "No hi ha cap programari que coincideixi amb les vostres paraules clau."

#: software.html:360
msgid "The Boutique is just a collection of software."
msgstr "La botiga només és una col·lecció de programari."

#: software.html:362
msgid "Install a software center"
msgstr "Instal·la un centre de programari"

#: software.html:364
msgid "to explore the entire Ubuntu catalogue."
msgstr "per explorar el catàleg complet d'Ubuntu."

#: software.html:367
msgid "Search again and include proprietary software."
msgstr "Cerca de nou i inclou el programari propietari."

#: software.html:376
msgid "Fine-tune the Software Boutique to suit your preferred behaviors."
msgstr "Afineu la botiga de programari perquè us satisfaci el seu comportament."

#: software.html:379
msgid "Boutique Selection"
msgstr "Selecció de la botiga"

#: software.html:381
msgid "Hide proprietary applications."
msgstr "Oculta les aplicacions propietàries."

#: software.html:385
msgid "Installation"
msgstr "Instal·lació"

#: software.html:387
msgid "Enable the Bulk Queue."
msgstr "Habilita la cua."

#: software.html:388
msgid ""
"Applications wait to be installed/removed, allowing you to continue "
"browsing."
msgstr "Les aplicacions esperen per ser instal·lades o suprimides, el que us permet continuar amb la navegació."

#: software.html:393
msgid "Suppress Progress Dialog"
msgstr "Suprimeix el diàleg de progrés"

#: software.html:394
msgid "Install/remove applications in the background."
msgstr "Instal·la o suprimeix les aplicacions en segon pla."

#: software.html:401
msgid "Your Queued Changes"
msgstr "La vostra cua de canvis"

#: software.html:402
msgid ""
"Decide which software to add or remove without committing straight away."
msgstr "Decidiu quin programari s'afegeix o es treu sense cometre-ho ara mateix."

#: software.html:406
msgid "Apply Changes"
msgstr "Aplica els canvis"

#: software.html:407
msgid "Clear"
msgstr "Neteja"

#: software.html:408
msgid "Finished"
msgstr "Finalitzat"

#: software.html:419
msgid "Not all changes could be applied."
msgstr "No tots els canvis podrien aplicar-se."

#: software.html:420
msgid "Please review the error dialog for further details."
msgstr "Si us plau, reviseu el diàleg d'error per a més detalls."

#: software.html:424
msgid "Nothing to see here -- There's no software in the queue yet."
msgstr "No hi ha res a veure aquí -- Encara no hi ha programari a la cua."

#: software.html:435
msgid "Filter by Software License"
msgstr "Filtra per la llicència del programari"

#: software.html:436
msgid "Hide Proprietary Software"
msgstr "Oculta el programari propietari"

#: software.html:438
msgid "Show Terminal Commands"
msgstr "Mostra les ordres del terminal"

#: software.html:444
msgid "Proprietary Software"
msgstr "El programari propietari"

#: software.html:445
msgid ""
"is owned by an individual or company. There may be restrictions on its usage"
" (like a license agreement), and provides no access to the source code."
msgstr "és propietat d'una persona o empresa. Hi pot haver restriccions al seu ús (com un acord de llicència), i no proporciona accés al codi font."

#: software.html:448
msgid "Free and Open Source Software"
msgstr "El programari de codi lliure i obert"

#: software.html:449
msgid ""
"grants the user the freedom to share, study and modify the software without "
"restriction. For instance, the GPL is an example of a 'Free Software' "
"license."
msgstr "atorga a l'usuari la llibertat de compartir, estudiar i modificar el programari sense restriccions. Per exemple, la GPL és un exemple d'una llicència de «programari lliure»."

#: software.html:49
msgid "Stocking the Boutique..."
msgstr "S'estan afegint els productes a la botiga..."

#: software.html413, 415
msgid "Advanced"
msgstr "Avançat"

#: software.html:417
msgid "Show technical package details."
msgstr "Mostra els detalls tècnics del paquet."

#: software.html:418
msgid ""
"Displays an estimated install size, packages and the type of application"
msgstr "Mostra una mida aproximada de la instal·lació, els paquets i el tipus d'aplicació"

#: software.html:424
msgid "Show Software Sources"
msgstr "Mostra els orígens de programari"

#: software.html425, 470
msgid "Manage Repositories"
msgstr "Gestiona els dipòsits"

#: software.html:460
msgid "Software Sources"
msgstr "Orígens de programari"

#: software.html:461
msgid ""
"Your system and applications are obtained and kept up-to-date via the "
"following repositories."
msgstr "El vostre sistema i les aplicacions s'obtenen i es mantenen actualitzats a través dels següents dipòsits."

#: software.html:463
msgid "Parsing lists..."
msgstr "S'estan analitzant les llistes..."

#: software.html:471
msgid "Update Package Lists"
msgstr "Actualitza les llistes dels paquets"

#: software.html:494
msgid "About Proprietary Software"
msgstr "Quant al programari propietari"
